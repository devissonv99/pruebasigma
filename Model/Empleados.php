<?php
class empleados{
    private $pdo;
    public $Departamento;
    public $Ciudad;
    public $Nombres;
    public $Correo;

// Creamos la funcion constructor para utilizar la conexion a la base de datos
// Una sola vez
public function __CONSTRUCT()
{
    try 
    {
        $this->pdo=Database::StartUp();
    }catch (Exception $e) {
        die($e->getMessage());
    }
}

public function listarDepartamentos()
{
    try{

        $stmt = file_get_contents('https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json');        
        $stmt2 = json_decode($stmt);
        return ($stmt2);
    }catch (Exception $e) {
        die($e->getMessage());
    }
}

public function registrar(Empleados $data)
{
    try {
        $sql = "INSERT INTO contacts (name,email,state,city)
                VALUES (?,?,?,?)";

        $this->pdo->prepare($sql)->execute(
            array(
                $data->Nombres,
                $data->Correo,
                $data->Departamento,
                $data->Ciudad
            )
            );     

    }catch (Exception $e) {
        die($e->getMessage());
    }
}


}
?>