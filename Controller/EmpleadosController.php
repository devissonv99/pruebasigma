<?php
    // Requerimos una sola vez el modelo que nos tiene toda la logica de negocio 
    // del sitio web
    require_once 'Model/Empleados.php';

    // Creamos la clase para el controlador
    class EmpleadosController
    {

        // Creamos una variable tipo privado en la cual por medio del constructor
        // le vamos a almacenar la clase del modelo
        // la cual tiene toda la logica de negocio
        private $model;

        // Creamos la funcion construct para utilizar solo una vez los metodos del modelo
        public function __CONSTRUCT()
        {
            $this->model = new empleados();
        }

        // Creamos la funcion index para requerir las vistas
        public function index()
        {
            require_once 'views/header.php';
            require_once 'views/empleado/empleado.php';
        }

        // public function Crud()
        // {
        //     $empleados = new empleados();

        //     // Creamo un if donde decimos que si IdEmpleado esta defenido y que no sea null
        //     if(isset($_REQUEST['IdEmpleado'])){
        //         // Si la condicion se cumple va al modelo y al metodo obtener
        //         // y busca el IdEmpleado
        //         $empleados = $this->model->obtener($_REQUEST['IdEmpleado']);
        //     }

        //     require_once 'views/header.php';
        //     require_once 'views/empleado/EmpleadoEditar.php';
        // }

        public function guardar()
        {
            $empleados = new empleados();

            $empleados->Nombres = $_REQUEST['Nombres'];
            $empleados->Correo = $_REQUEST['Correo'];
            $empleados->Departamento = $_REQUEST['Departamento'];
            $empleados->Ciudad = $_REQUEST['Ciudad'];

            
            $this->model->Registrar($empleados);
            
            header('Location: index.php');
        }

    }
?>