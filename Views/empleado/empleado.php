<div class="row">
        <div class="col-4">
          <div class="text-center">
            <img src="assets/img/logosigma.png" style="text-align:center;" class="rounded float-right logo" alt="Responsive image">
            <h2>Prueba de desarrollo sigma</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore, vero voluptatum sed velit ab rerum molestias ducimus delectus modi? Assumenda obcaecati aut dolorum voluptates animi quam rerum deserunt explicabo doloribus!</p>
          </div>     
        </div>

</div>


<div class="modal-body row" >
     <div class="col-md-6" id="colIzq">
       <!-- Your first column here -->
        <img src="assets/img/sigma-image.png" style="height: 100%;width: 100%;" >
     </div>
     <div class="col-md-6" id="colDer">
       <!-- Your second column here -->
        <form id="frm-alumno" action="?e=Empleados&a=guardar" method="post" enctype="multipart/form-data">
        <br>
        <br>
            <div class="form-group">
                <label>Departamento*</label>
                <select name="Departamento" onchange="buscarCiudades()" id="Departamento" class="select-css" required >
                    <option value="">Departamento</option>
                    <?php foreach($this->model->listarDepartamentos() as $c=>$d): ?>
                        <option value="<?php echo $c; ?>"><?php echo $c; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
    
            <div class="form-group">
                <label>Ciudad*</label>
                <div  id="Ciu">
                    <select name="Ciudad" id="Ciudad" class="select-css" required>
                    <option value="">Ciudad</option>
                    </select>
                </div>
                
            </div>
            
            <div class="form-group">
                <label>Nombre*</label>
                <input type="text" name="Nombres" id="Nombres" value="" class="form-control" placeholder="Nombre" required>
            </div>

            <div class="form-group">
                <label>Correo:</label>
                <input type="email" name="Correo" id="Correo" value="" class="form-control" placeholder="Correo" required>
            </div>

            <div class="text-right">
                <button class="btn btnguardar" onclick="Validar()">Enviar</button>
            </div>
            <br>
        </form>

     </div>
</div>

